<?php

declare(strict_types=1);

use Composer\Autoload\ClassLoader;

if (!function_exists('base_path')) {
    function base_path(string $path = ''): string
    {
        $refLoader = new ReflectionClass(ClassLoader::class);
        $classLoadPath = $refLoader->getFileName() ?: __FILE__;
        $basePath = dirname($classLoadPath, 3);

        return $path ? "$basePath/$path" : $basePath;
    }
}

if (!function_exists('public_path')) {
    function public_path(string $path = ''): string
    {
        return base_path('public' . ($path ? "/$path" : ''));
    }
}

if (!function_exists('resource_path')) {
    function resource_path(string $path = ''): string
    {
        return base_path('resources' . ($path ? "/$path" : ''));
    }
}

if (!function_exists('lang_path')) {
    function lang_path(string $path = ''): string
    {
        return resource_path('lang' . ($path ? "/$path" : ''));
    }
}

if (!function_exists('storage_path')) {
    function storage_path(string $path = ''): string
    {
        return base_path('storage' . ($path ? "/$path" : ''));
    }
}

if (!function_exists('app_path')) {
    function app_path(string $path = ''): string
    {
        return base_path('src' . ($path ? "/$path" : ''));
    }
}

if (!function_exists('config_path')) {
    function config_path(string $path = ''): string
    {
        return base_path('config' . ($path ? "/$path" : ''));
    }
}

if (!function_exists('database_path')) {
    function database_path(string $path = ''): string
    {
        return base_path('database' . ($path ? "/$path" : ''));
    }
}
