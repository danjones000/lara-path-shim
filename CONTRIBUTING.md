# Contributing

Contributions are **welcome**.

We accept contributions via Pull Requests on [Codeberg](https://codeberg.org/danjones000/lara-path-shim).

## Pull Requests

- **PSR-12** - Running `composer phpcs` will ensure all code is following PSR-12.

- **Add tests!** - to ensure a high quality code base, your code patch can't be accepted if it doesn't have tests.

- **Document any change in behaviour** - Make sure the `CHANGELOG.md`, `README.md` and any other relevant documentation are kept up-to-date.

- **Consider our release cycle** - Since we're using semantic versioning ([SemVer v2.0.0](http://semver.org/)). Changes to the API must be done with great consideration, and prevented if at all possible.

- **Create dedicated branches** - `stable` is the stable branch, create a new branch for each subject.
    + We follow Git Flow, so your branch should, ideally, be named `feature/some-feature` or `bugfix/some-bug`.
    + Pull Request should target the `develop` branch, rather `stable`.

- **One pull request per subject** - If you want to do more than one thing, send multiple pull requests.

- **Choose good names for the pull request** - Be descriptive in choosing the branch name, for instance, the type of pull request. As examples: feature/my-pull-request-title, hotfix/my-pull-request-title, bugfix/my-pull-request-title, rollback/my-pull-request-title, etc.

- **Be respectful** - Be excellent to other contributors.

**Happy coding**!
