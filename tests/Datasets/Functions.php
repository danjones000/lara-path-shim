<?php

declare(strict_types=1);

dataset('functions', function () {
    $base = dirname(__FILE__, 3);

    return [
        'base'   => ['base_path', $base],
        'app'    => ['app_path', "$base/src"],
        'config' => ['config_path', "$base/config"],
        'db'     => ['database_path', "$base/database"],
        'res'    => ['resource_path', "$base/resources"],
        'lang'   => ['lang_path', "$base/resources/lang"],
        'public' => ['public_path', "$base/public"],
        'store'  => ['storage_path', "$base/storage"],
    ];
});
