<?php

declare(strict_types=1);

test('functions exist', function (string $name) {
    expect(function_exists($name))->toBeTrue();
})->with('functions');

test('function returns correct directory', function (string $name, string $path) {
    expect($name())->toBe($path);
})->with('functions');

test('function returns added path', function (string $name, string $path) {
    $dir = uniqid();

    expect($name($dir))->toBe("$path/$dir");
})->with('functions');
