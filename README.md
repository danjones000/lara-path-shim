# Laravel Path Shim

This library provides a shim for the various `*_path` functions provided by Laravel.

This is mostly useful when developing Laravel libraries that depend on these functions, but you don't want to have to install a full Laravel installation for development.

## Installation

`composer require --dev danjones000/lara-path-shim`

Do not install this with Laravel. It may cause problems.
